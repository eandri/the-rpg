using System;
using System.Collections;
using System.Collections.Generic;
using RPG.Core;
using RPG.Movement;
using UnityEngine;

namespace RPG.Combat
{
    public class Fighter : MonoBehaviour, IAction
    {
        private Mover _mover;
        [SerializeField] private float _weaponRange = 2f;
        
        private Transform target;

        private void Start()
        {
            _mover = GetComponent<Mover>();
        }

        private void Update()
        {
            if (target == null) return;
            
            if (!IsInRange())
            {
                _mover.MoveTo(target.position);
                
            }
            else
            {
                _mover.Cancel();
                AttackBehaviour();
            }
        }

        private void AttackBehaviour()
        {
            GetComponent<Animator>().SetTrigger("attack");
        }

        private bool IsInRange()
        {
            return Vector3.Distance(transform.position, target.position) < _weaponRange;
        }

        public void Attack(CombatTarget target)
        {
            GetComponent<StateMachine>().StartAction(this);
            this.target = target.transform;
        }

        public void Cancel()
        {
            target = null;
        }
        //Animation Event
        void Hit()
        {
            
        }
    }

}

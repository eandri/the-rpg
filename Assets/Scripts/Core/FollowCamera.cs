using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    public class FollowCamera : MonoBehaviour
    {
        [SerializeField] private Transform _cameraTarget;
    
        void Start()
        {
        
        }

    
        void LateUpdate()
        {
            transform.position = _cameraTarget.position;
        }
    }

}
